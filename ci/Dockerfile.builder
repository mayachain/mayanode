FROM golang:1.22.1

ENV CGO_ENABLED=1

# hadolint ignore=DL3008,DL4006
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    curl git jq make protobuf-compiler xz-utils sudo python3-pip \
    # required packages so that we can build for musl libc
    musl-tools linux-headers-amd64 build-essential \
    && rm -rf /var/cache/apt/lists \
    && go install mvdan.cc/gofumpt@v0.5.0

# Add the required kernel headers symlinks for musl libc
RUN ln -s /usr/include/linux /usr/include/x86_64-linux-musl/ \
    && ln -s /usr/include/asm-generic /usr/include/x86_64-linux-musl/ \
    && ln -s /usr/include/x86_64-linux-gnu/asm /usr/include/x86_64-linux-musl/
