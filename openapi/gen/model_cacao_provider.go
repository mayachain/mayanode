/*
Mayanode API

Mayanode REST API.

Contact: devs@mayachain.org
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// CACAOProvider struct for CACAOProvider
type CACAOProvider struct {
	CacaoAddress string `json:"cacao_address"`
	Units string `json:"units"`
	Value string `json:"value"`
	Pnl string `json:"pnl"`
	DepositAmount string `json:"deposit_amount"`
	WithdrawAmount string `json:"withdraw_amount"`
	LastDepositHeight int64 `json:"last_deposit_height"`
	LastWithdrawHeight int64 `json:"last_withdraw_height"`
}

// NewCACAOProvider instantiates a new CACAOProvider object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCACAOProvider(cacaoAddress string, units string, value string, pnl string, depositAmount string, withdrawAmount string, lastDepositHeight int64, lastWithdrawHeight int64) *CACAOProvider {
	this := CACAOProvider{}
	this.CacaoAddress = cacaoAddress
	this.Units = units
	this.Value = value
	this.Pnl = pnl
	this.DepositAmount = depositAmount
	this.WithdrawAmount = withdrawAmount
	this.LastDepositHeight = lastDepositHeight
	this.LastWithdrawHeight = lastWithdrawHeight
	return &this
}

// NewCACAOProviderWithDefaults instantiates a new CACAOProvider object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCACAOProviderWithDefaults() *CACAOProvider {
	this := CACAOProvider{}
	return &this
}

// GetCacaoAddress returns the CacaoAddress field value
func (o *CACAOProvider) GetCacaoAddress() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.CacaoAddress
}

// GetCacaoAddressOk returns a tuple with the CacaoAddress field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetCacaoAddressOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CacaoAddress, true
}

// SetCacaoAddress sets field value
func (o *CACAOProvider) SetCacaoAddress(v string) {
	o.CacaoAddress = v
}

// GetUnits returns the Units field value
func (o *CACAOProvider) GetUnits() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Units
}

// GetUnitsOk returns a tuple with the Units field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetUnitsOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Units, true
}

// SetUnits sets field value
func (o *CACAOProvider) SetUnits(v string) {
	o.Units = v
}

// GetValue returns the Value field value
func (o *CACAOProvider) GetValue() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Value
}

// GetValueOk returns a tuple with the Value field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetValueOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Value, true
}

// SetValue sets field value
func (o *CACAOProvider) SetValue(v string) {
	o.Value = v
}

// GetPnl returns the Pnl field value
func (o *CACAOProvider) GetPnl() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Pnl
}

// GetPnlOk returns a tuple with the Pnl field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetPnlOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Pnl, true
}

// SetPnl sets field value
func (o *CACAOProvider) SetPnl(v string) {
	o.Pnl = v
}

// GetDepositAmount returns the DepositAmount field value
func (o *CACAOProvider) GetDepositAmount() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.DepositAmount
}

// GetDepositAmountOk returns a tuple with the DepositAmount field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetDepositAmountOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.DepositAmount, true
}

// SetDepositAmount sets field value
func (o *CACAOProvider) SetDepositAmount(v string) {
	o.DepositAmount = v
}

// GetWithdrawAmount returns the WithdrawAmount field value
func (o *CACAOProvider) GetWithdrawAmount() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.WithdrawAmount
}

// GetWithdrawAmountOk returns a tuple with the WithdrawAmount field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetWithdrawAmountOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.WithdrawAmount, true
}

// SetWithdrawAmount sets field value
func (o *CACAOProvider) SetWithdrawAmount(v string) {
	o.WithdrawAmount = v
}

// GetLastDepositHeight returns the LastDepositHeight field value
func (o *CACAOProvider) GetLastDepositHeight() int64 {
	if o == nil {
		var ret int64
		return ret
	}

	return o.LastDepositHeight
}

// GetLastDepositHeightOk returns a tuple with the LastDepositHeight field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetLastDepositHeightOk() (*int64, bool) {
	if o == nil {
		return nil, false
	}
	return &o.LastDepositHeight, true
}

// SetLastDepositHeight sets field value
func (o *CACAOProvider) SetLastDepositHeight(v int64) {
	o.LastDepositHeight = v
}

// GetLastWithdrawHeight returns the LastWithdrawHeight field value
func (o *CACAOProvider) GetLastWithdrawHeight() int64 {
	if o == nil {
		var ret int64
		return ret
	}

	return o.LastWithdrawHeight
}

// GetLastWithdrawHeightOk returns a tuple with the LastWithdrawHeight field value
// and a boolean to check if the value has been set.
func (o *CACAOProvider) GetLastWithdrawHeightOk() (*int64, bool) {
	if o == nil {
		return nil, false
	}
	return &o.LastWithdrawHeight, true
}

// SetLastWithdrawHeight sets field value
func (o *CACAOProvider) SetLastWithdrawHeight(v int64) {
	o.LastWithdrawHeight = v
}

func (o CACAOProvider) MarshalJSON_deprecated() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["cacao_address"] = o.CacaoAddress
	}
	if true {
		toSerialize["units"] = o.Units
	}
	if true {
		toSerialize["value"] = o.Value
	}
	if true {
		toSerialize["pnl"] = o.Pnl
	}
	if true {
		toSerialize["deposit_amount"] = o.DepositAmount
	}
	if true {
		toSerialize["withdraw_amount"] = o.WithdrawAmount
	}
	if true {
		toSerialize["last_deposit_height"] = o.LastDepositHeight
	}
	if true {
		toSerialize["last_withdraw_height"] = o.LastWithdrawHeight
	}
	return json.Marshal(toSerialize)
}

type NullableCACAOProvider struct {
	value *CACAOProvider
	isSet bool
}

func (v NullableCACAOProvider) Get() *CACAOProvider {
	return v.value
}

func (v *NullableCACAOProvider) Set(val *CACAOProvider) {
	v.value = val
	v.isSet = true
}

func (v NullableCACAOProvider) IsSet() bool {
	return v.isSet
}

func (v *NullableCACAOProvider) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCACAOProvider(val *CACAOProvider) *NullableCACAOProvider {
	return &NullableCACAOProvider{value: val, isSet: true}
}

func (v NullableCACAOProvider) MarshalJSON_deprecated() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCACAOProvider) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


