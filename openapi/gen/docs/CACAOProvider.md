# CACAOProvider

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CacaoAddress** | **string** |  | 
**Units** | **string** |  | 
**Value** | **string** |  | 
**Pnl** | **string** |  | 
**DepositAmount** | **string** |  | 
**WithdrawAmount** | **string** |  | 
**LastDepositHeight** | **int64** |  | 
**LastWithdrawHeight** | **int64** |  | 

## Methods

### NewCACAOProvider

`func NewCACAOProvider(cacaoAddress string, units string, value string, pnl string, depositAmount string, withdrawAmount string, lastDepositHeight int64, lastWithdrawHeight int64, ) *CACAOProvider`

NewCACAOProvider instantiates a new CACAOProvider object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCACAOProviderWithDefaults

`func NewCACAOProviderWithDefaults() *CACAOProvider`

NewCACAOProviderWithDefaults instantiates a new CACAOProvider object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCacaoAddress

`func (o *CACAOProvider) GetCacaoAddress() string`

GetCacaoAddress returns the CacaoAddress field if non-nil, zero value otherwise.

### GetCacaoAddressOk

`func (o *CACAOProvider) GetCacaoAddressOk() (*string, bool)`

GetCacaoAddressOk returns a tuple with the CacaoAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCacaoAddress

`func (o *CACAOProvider) SetCacaoAddress(v string)`

SetCacaoAddress sets CacaoAddress field to given value.


### GetUnits

`func (o *CACAOProvider) GetUnits() string`

GetUnits returns the Units field if non-nil, zero value otherwise.

### GetUnitsOk

`func (o *CACAOProvider) GetUnitsOk() (*string, bool)`

GetUnitsOk returns a tuple with the Units field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnits

`func (o *CACAOProvider) SetUnits(v string)`

SetUnits sets Units field to given value.


### GetValue

`func (o *CACAOProvider) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *CACAOProvider) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *CACAOProvider) SetValue(v string)`

SetValue sets Value field to given value.


### GetPnl

`func (o *CACAOProvider) GetPnl() string`

GetPnl returns the Pnl field if non-nil, zero value otherwise.

### GetPnlOk

`func (o *CACAOProvider) GetPnlOk() (*string, bool)`

GetPnlOk returns a tuple with the Pnl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnl

`func (o *CACAOProvider) SetPnl(v string)`

SetPnl sets Pnl field to given value.


### GetDepositAmount

`func (o *CACAOProvider) GetDepositAmount() string`

GetDepositAmount returns the DepositAmount field if non-nil, zero value otherwise.

### GetDepositAmountOk

`func (o *CACAOProvider) GetDepositAmountOk() (*string, bool)`

GetDepositAmountOk returns a tuple with the DepositAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDepositAmount

`func (o *CACAOProvider) SetDepositAmount(v string)`

SetDepositAmount sets DepositAmount field to given value.


### GetWithdrawAmount

`func (o *CACAOProvider) GetWithdrawAmount() string`

GetWithdrawAmount returns the WithdrawAmount field if non-nil, zero value otherwise.

### GetWithdrawAmountOk

`func (o *CACAOProvider) GetWithdrawAmountOk() (*string, bool)`

GetWithdrawAmountOk returns a tuple with the WithdrawAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWithdrawAmount

`func (o *CACAOProvider) SetWithdrawAmount(v string)`

SetWithdrawAmount sets WithdrawAmount field to given value.


### GetLastDepositHeight

`func (o *CACAOProvider) GetLastDepositHeight() int64`

GetLastDepositHeight returns the LastDepositHeight field if non-nil, zero value otherwise.

### GetLastDepositHeightOk

`func (o *CACAOProvider) GetLastDepositHeightOk() (*int64, bool)`

GetLastDepositHeightOk returns a tuple with the LastDepositHeight field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastDepositHeight

`func (o *CACAOProvider) SetLastDepositHeight(v int64)`

SetLastDepositHeight sets LastDepositHeight field to given value.


### GetLastWithdrawHeight

`func (o *CACAOProvider) GetLastWithdrawHeight() int64`

GetLastWithdrawHeight returns the LastWithdrawHeight field if non-nil, zero value otherwise.

### GetLastWithdrawHeightOk

`func (o *CACAOProvider) GetLastWithdrawHeightOk() (*int64, bool)`

GetLastWithdrawHeightOk returns a tuple with the LastWithdrawHeight field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastWithdrawHeight

`func (o *CACAOProvider) SetLastWithdrawHeight(v int64)`

SetLastWithdrawHeight sets LastWithdrawHeight field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


