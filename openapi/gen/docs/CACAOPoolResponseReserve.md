# CACAOPoolResponseReserve

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Units** | **string** | the units of CACAOPool owned by the reserve | 
**Value** | **string** | the value of the reserve share of the CACAOPool | 
**Pnl** | **string** | the profit and loss of the reserve share of the CACAOPool | 
**CurrentDeposit** | **string** | the current CACAO deposited by the reserve | 

## Methods

### NewCACAOPoolResponseReserve

`func NewCACAOPoolResponseReserve(units string, value string, pnl string, currentDeposit string, ) *CACAOPoolResponseReserve`

NewCACAOPoolResponseReserve instantiates a new CACAOPoolResponseReserve object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCACAOPoolResponseReserveWithDefaults

`func NewCACAOPoolResponseReserveWithDefaults() *CACAOPoolResponseReserve`

NewCACAOPoolResponseReserveWithDefaults instantiates a new CACAOPoolResponseReserve object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUnits

`func (o *CACAOPoolResponseReserve) GetUnits() string`

GetUnits returns the Units field if non-nil, zero value otherwise.

### GetUnitsOk

`func (o *CACAOPoolResponseReserve) GetUnitsOk() (*string, bool)`

GetUnitsOk returns a tuple with the Units field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnits

`func (o *CACAOPoolResponseReserve) SetUnits(v string)`

SetUnits sets Units field to given value.


### GetValue

`func (o *CACAOPoolResponseReserve) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *CACAOPoolResponseReserve) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *CACAOPoolResponseReserve) SetValue(v string)`

SetValue sets Value field to given value.


### GetPnl

`func (o *CACAOPoolResponseReserve) GetPnl() string`

GetPnl returns the Pnl field if non-nil, zero value otherwise.

### GetPnlOk

`func (o *CACAOPoolResponseReserve) GetPnlOk() (*string, bool)`

GetPnlOk returns a tuple with the Pnl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnl

`func (o *CACAOPoolResponseReserve) SetPnl(v string)`

SetPnl sets Pnl field to given value.


### GetCurrentDeposit

`func (o *CACAOPoolResponseReserve) GetCurrentDeposit() string`

GetCurrentDeposit returns the CurrentDeposit field if non-nil, zero value otherwise.

### GetCurrentDepositOk

`func (o *CACAOPoolResponseReserve) GetCurrentDepositOk() (*string, bool)`

GetCurrentDepositOk returns a tuple with the CurrentDeposit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentDeposit

`func (o *CACAOPoolResponseReserve) SetCurrentDeposit(v string)`

SetCurrentDeposit sets CurrentDeposit field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


