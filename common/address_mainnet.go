//go:build !testnet && !mocknet
// +build !testnet,!mocknet

package common

var (
	RadixAccountAddressPrefix = "account_rdx1"
	RadixPrefixAbbreviations  = []string{"ardx1", "rdx1", "1"}
)
