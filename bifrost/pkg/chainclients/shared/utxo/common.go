package utxo

import (
	"fmt"

	"gitlab.com/mayachain/mayanode/bifrost/mayaclient"
	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/constants"
)

func GetConfMulBasisPoint(chain string, bridge mayaclient.MayachainBridge) (cosmos.Uint, error) {
	confMulKey := fmt.Sprintf("ConfMultiplierBasisPoints-%s", chain)
	confMultiplier, err := bridge.GetMimir(confMulKey)
	// should never be negative
	if err != nil || confMultiplier <= 0 {
		return cosmos.NewUint(constants.MaxBasisPts), err
	}
	return cosmos.NewUint(uint64(confMultiplier)), nil
}

func MaxConfAdjustment(confirm uint64, chain string, bridge mayaclient.MayachainBridge) (uint64, error) {
	maxConfKey := fmt.Sprintf("MaxConfirmations-%s", chain)
	maxConfirmations, err := bridge.GetMimir(maxConfKey)
	if err != nil || maxConfirmations <= 0 {
		return confirm, err
	}
	if maxConfirmations > 0 && confirm > uint64(maxConfirmations) {
		confirm = uint64(maxConfirmations)
	}
	return confirm, nil
}

func GetAsgardAddresses(chain common.Chain, bridge mayaclient.MayachainBridge) ([]common.Address, error) {
	vaults, err := bridge.GetAsgardPubKeys()
	if err != nil {
		return nil, fmt.Errorf("fail to get asgards : %w", err)
	}

	newAddresses := make([]common.Address, 0)
	for _, v := range vaults {
		var addr common.Address
		addr, err = v.PubKey.GetAddress(chain)
		if err != nil {
			continue
		}
		newAddresses = append(newAddresses, addr)
	}
	return newAddresses, nil
}
