package mayachain

import (
	"strings"

	"gitlab.com/mayachain/mayanode/common"
	cosmos "gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/constants"
)

// "pool+"

type CacaoPoolDepositMemo struct {
	MemoBase
}

func (m CacaoPoolDepositMemo) String() string {
	return m.string(false)
}

func (m CacaoPoolDepositMemo) ShortString() string {
	return m.string(true)
}

func (m CacaoPoolDepositMemo) string(short bool) string {
	return "pool+"
}

func NewCacaoPoolDepositMemo() CacaoPoolDepositMemo {
	return CacaoPoolDepositMemo{
		MemoBase: MemoBase{TxType: TxCacaoPoolDeposit},
	}
}

func (p *parser) ParseCacaoPoolDepositMemo() (CacaoPoolDepositMemo, error) {
	return NewCacaoPoolDepositMemo(), nil
}

// "pool-:<basis-points>:<affiliate>:<affiliate-basis-points>"

type CacaoPoolWithdrawMemo struct {
	MemoBase
	BasisPoints          cosmos.Uint
	AffiliateAddress     common.Address
	AffiliateBasisPoints cosmos.Uint
}

func (m CacaoPoolWithdrawMemo) GetBasisPts() cosmos.Uint             { return m.BasisPoints }
func (m CacaoPoolWithdrawMemo) GetAffiliateAddress() common.Address  { return m.AffiliateAddress }
func (m CacaoPoolWithdrawMemo) GetAffiliateBasisPoints() cosmos.Uint { return m.AffiliateBasisPoints }

func (m CacaoPoolWithdrawMemo) String() string {
	args := []string{TxCacaoPoolWithdraw.String(), m.BasisPoints.String(), m.AffiliateAddress.String(), m.AffiliateBasisPoints.String()}
	return strings.Join(args, ":")
}

func NewCacaoPoolWithdrawMemo(basisPoints cosmos.Uint, affAddr common.Address, affBps cosmos.Uint) CacaoPoolWithdrawMemo {
	return CacaoPoolWithdrawMemo{
		MemoBase:             MemoBase{TxType: TxCacaoPoolWithdraw},
		BasisPoints:          basisPoints,
		AffiliateAddress:     affAddr,
		AffiliateBasisPoints: affBps,
	}
}

func (p *parser) ParseCacaoPoolWithdrawMemo() (CacaoPoolWithdrawMemo, error) {
	basisPoints := p.getUint(1, true, cosmos.ZeroInt().Uint64())
	affiliateAddress := p.getAddressWithKeeper(2, false, common.NoAddress, common.BASEChain, p.version)
	affiliateBasisPoints := p.getUintWithMaxValue(3, false, 0, constants.MaxBasisPts)
	return NewCacaoPoolWithdrawMemo(basisPoints, affiliateAddress, affiliateBasisPoints), p.Error()
}
