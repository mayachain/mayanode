//go:build regtest
// +build regtest

package mayachain

import (
	"fmt"
	"strconv"
	"strings"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
)

func migrateStoreV86(ctx cosmos.Context, mgr *Mgrs)    {}
func migrateStoreV88(ctx cosmos.Context, mgr Manager)  {}
func migrateStoreV90(ctx cosmos.Context, mgr Manager)  {}
func migrateStoreV96(ctx cosmos.Context, mgr Manager)  {}
func migrateStoreV102(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV104(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV105(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV106(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV107(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV108(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV109(ctx cosmos.Context, mgr Manager) {}
func migrateStoreV110(ctx cosmos.Context, mgr Manager) {}

func migrateStoreV111(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v111", "error", err)
		}
	}()

	// For any in-progress streaming swaps to non-RUNE Native coins,
	// mint the current Out amount to the Pool Module.
	var coinsToMint common.Coins

	iterator := mgr.Keeper().GetSwapQueueIterator(ctx)
	defer iterator.Close()
	for ; iterator.Valid(); iterator.Next() {
		var msg MsgSwap
		if err := mgr.Keeper().Cdc().Unmarshal(iterator.Value(), &msg); err != nil {
			ctx.Logger().Error("fail to fetch swap msg from queue", "error", err)
			continue
		}

		if !msg.IsStreaming() || !msg.TargetAsset.IsNative() || msg.TargetAsset.IsBase() {
			continue
		}

		swp, err := mgr.Keeper().GetStreamingSwap(ctx, msg.Tx.ID)
		if err != nil {
			ctx.Logger().Error("fail to fetch streaming swap", "error", err)
			continue
		}

		if !swp.Out.IsZero() {
			mintCoin := common.NewCoin(msg.TargetAsset, swp.Out)
			coinsToMint = coinsToMint.Add(mintCoin)
		}
	}

	// The minted coins are for in-progress swaps, so keeping the "swap" in the event field and logs.
	var coinsToTransfer common.Coins
	for _, mintCoin := range coinsToMint {
		if err := mgr.Keeper().MintToModule(ctx, ModuleName, mintCoin); err != nil {
			ctx.Logger().Error("fail to mint coins during swap", "error", err)
		} else {
			// MintBurn event is not currently implemented, will ignore

			// mintEvt := NewEventMintBurn(MintSupplyType, mintCoin.Asset.Native(), mintCoin.Amount, "swap")
			// if err := mgr.EventMgr().EmitEvent(ctx, mintEvt); err != nil {
			// 	ctx.Logger().Error("fail to emit mint event", "error", err)
			// }
			coinsToTransfer = coinsToTransfer.Add(mintCoin)
		}
	}

	if err := mgr.Keeper().SendFromModuleToModule(ctx, ModuleName, AsgardName, coinsToTransfer); err != nil {
		ctx.Logger().Error("fail to move coins during swap", "error", err)
	}
}

func migrateStoreV112(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v112", "error", err)
		}
	}()

	// Mock ObservedTxVoter state from
	// https://mayanode.mayachain.info/mayachain/tx/details/6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB
	tx6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB := types.ObservedTxVoter{
		TxID: common.TxID("6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB"),
		Tx: types.ObservedTx{
			Tx: common.Tx{
				ID:          "6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
				Chain:       common.THORChain,
				FromAddress: common.Address("thor1kfqzcr8m73qd97z46wha2w8u6f22tj9dxyquj6"),
				Coins: common.Coins{
					{
						Asset:    common.RUNEAsset,
						Amount:   cosmos.NewUint(2000000000000),
						Decimals: 8,
					},
				},
				Gas: common.Gas{
					{
						Asset:  common.RUNEAsset,
						Amount: cosmos.NewUint(2000000),
					},
				},
				Memo: "=:ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48:0xAa287489e76B11B56dBa7ca03e155369400f3d65:9749038937200/3/0:ts:50",
			},
			ObservedPubKey: "tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx",
			Signers:        []string{""},
			OutHashes:      []string{"0000000000000000000000000000000000000000000000000000000000000000"},
			Status:         types.Status_done,
		},
		Txs: types.ObservedTxs{
			{
				Tx: common.Tx{
					ID:          "6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
					Chain:       common.THORChain,
					FromAddress: common.Address("thor1kfqzcr8m73qd97z46wha2w8u6f22tj9dxyquj6"),
					Coins: common.Coins{
						{
							Asset:    common.RUNEAsset,
							Amount:   cosmos.NewUint(2000000000000),
							Decimals: 8,
						},
					},
					Gas: common.Gas{
						{
							Asset:  common.RUNEAsset,
							Amount: cosmos.NewUint(2000000),
						},
					},
					Memo: "=:ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48:0xAa287489e76B11B56dBa7ca03e155369400f3d65:9749038937200/3/0:ts:50",
				},
				ObservedPubKey: "tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx",
				Signers:        []string{""},
				OutHashes:      []string{"0000000000000000000000000000000000000000000000000000000000000000"},
				Status:         types.Status_done,
			},
		},
		Actions: []types.TxOutItem{
			{
				Chain:     common.BASEChain,
				ToAddress: common.Address("maya1rm0xppz0ypgr3zqymnrdtnnjs4kpxqgy8tfmuk"),
				Coin: common.Coin{
					Asset:  common.BaseAsset(),
					Amount: cosmos.NewUint(8888139344845),
				},
				Memo: "OUT:6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
				MaxGas: common.Gas{
					{
						Asset:    common.BaseAsset(),
						Amount:   cosmos.ZeroUint(),
						Decimals: 8,
					},
				},
				GasRate: 2000000000,
				InHash:  common.TxID("6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB"),
			},
		},
		OutTxs: common.Txs{
			{
				ID:        common.TxID("0000000000000000000000000000000000000000000000000000000000000000"),
				Chain:     common.BASEChain,
				ToAddress: common.Address("maya1rm0xppz0ypgr3zqymnrdtnnjs4kpxqgy8tfmuk"),
				Coins: common.Coins{
					{
						Asset:  common.BaseAsset(),
						Amount: cosmos.NewUint(8888139344845),
					},
				},
				Gas: common.Gas{
					{
						Asset:  common.BaseAsset(),
						Amount: cosmos.NewUint(2000000000),
					},
				},
				Memo: "OUT:6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB",
			},
		},
		FinalisedHeight: 1,
		UpdatedVault:    true,
	}
	mgr.Keeper().SetObservedTxInVoter(ctx, tx6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB)

	// https://mayanode.mayachain.info/mayachain/tx/details/80559CC3CCF2665531AAA7DD6B59F986721C6B76F1DD056DAE58DCC4878C5D56
	// Tx doesn't have planned "actions" nor "out_txs", send it with TryAddTxOutItem()
	var err error
	originalTxID := "80559CC3CCF2665531AAA7DD6B59F986721C6B76F1DD056DAE58DCC4878C5D56"
	maxGas, err := mgr.gasMgr.GetMaxGas(ctx, common.THORChain)
	if err != nil {
		ctx.Logger().Error("unable to GetMaxGas while retrying issue 1", "err", err)
	} else {
		gasRate := mgr.gasMgr.GetGasRate(ctx, common.THORChain)
		droppedRescue := types.TxOutItem{
			Chain:       common.THORChain,
			ToAddress:   common.Address("thor1sucvdnzcf4j6ynep4n4skjpq8tvqv8ags3a4ky"),
			VaultPubKey: common.PubKey("tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx"),
			Coin: common.NewCoin(
				common.RUNEAsset,
				cosmos.NewUint(uint64(199757582857)),
			),
			Memo:    fmt.Sprintf("OUT:%s", originalTxID),
			InHash:  common.TxID(originalTxID),
			GasRate: int64(gasRate.Uint64()),
			MaxGas:  common.Gas{maxGas},
		}

		ok, err := mgr.txOutStore.TryAddTxOutItem(ctx, mgr, droppedRescue, cosmos.ZeroUint())
		if err != nil {
			ctx.Logger().Error("fail to retry THOR rescue tx", "error", err)
		}
		if !ok {
			ctx.Logger().Error("TryAddTxOutItem didn't success for tx")
		}
	}

	// https://mayanode.mayachain.info/mayachain/tx/details/6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB
	// Tx have planned "actions" but doesn't have "out_txs", send it with TryAddTxOutItem()
	originalTxID = "6F4F5801E5BEA96BC521BB00A7542EACB5FBDC161FD43431FEF3245D93F9C0AB"
	maxGas, err = mgr.gasMgr.GetMaxGas(ctx, common.ETHChain)
	if err != nil {
		ctx.Logger().Error("unable to GetMaxGas while retrying issue 1", "err", err)
	} else {
		gasRate := mgr.gasMgr.GetGasRate(ctx, common.ETHChain)
		droppedRescue := types.TxOutItem{
			Chain:       common.ETHChain,
			ToAddress:   common.Address("0xAa287489e76B11B56dBa7ca03e155369400f3d65"),
			VaultPubKey: common.PubKey("tmayapub1addwnpepqfshsq2y6ejy2ysxmq4gj8n8mzuzyulk9wh4n946jv5w2vpwdn2yuz3v0gx"), // regtests pubkey
			Coin: common.NewCoin(
				common.USDCAsset,
				// Check out value for https://mayanode.mayachain.info/mayachain/block?height=8292990
				cosmos.NewUint(uint64(9861109874700)),
			),
			Memo:    fmt.Sprintf("OUT:%s", originalTxID),
			InHash:  common.TxID(originalTxID),
			GasRate: int64(gasRate.Uint64()),
			MaxGas:  common.Gas{maxGas},
		}

		ok, err := mgr.txOutStore.TryAddTxOutItem(ctx, mgr, droppedRescue, cosmos.ZeroUint())
		if err != nil {
			ctx.Logger().Error("fail to retry THOR rescue tx", "error", err)
		}
		if !ok {
			ctx.Logger().Error("TryAddTxOutItem didn't success for tx")
		}
	}
}

func migrateStoreV113(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v113", "error", err)
		}
	}()

	txIds := common.TxIDs{}

	// Observed, but not paid txs
	observedTxIds := common.TxIDs{
		"431A6446957894DF77FA4220844990898F96BF46E1C8752910386A299AF72455",
	}

	for _, observedTxID := range observedTxIds {
		voter, err := mgr.K.GetObservedTxInVoter(ctx, observedTxID)
		if err != nil {
			ctx.Logger().Error("fail to get observed tx in voter", "error", err)
			continue
		}

		if len(voter.OutTxs) == 0 {
			continue
		}

		outboundTxID := voter.OutTxs[0].ID
		outVoter, err := mgr.K.GetObservedTxOutVoter(ctx, outboundTxID)
		if err != nil {
			ctx.Logger().Error("fail to get observed tx out voter", "error", err)
			continue
		}

		outVoter.SetReverted()
		voter.OutTxs = nil

		activeAsgards, err := mgr.Keeper().GetAsgardVaultsByStatus(ctx, ActiveVault)
		if err != nil || len(activeAsgards) == 0 {
			ctx.Logger().Error("fail to get active asgard vaults", "error", err)
			return
		}

		// we actually know there's only one active asgard
		if len(voter.Actions) > 0 {
			coin := voter.Actions[0].Coin
			gas := voter.Actions[0].MaxGas.ToCoins().GetCoin(coin.Asset).Amount
			coin.Amount = coin.Amount.Add(gas)
			activeAsgards[0].AddFunds(common.Coins{coin})

			// add the amount back to the vault
			if err := mgr.Keeper().SetVault(ctx, activeAsgards[0]); err != nil {
				ctx.Logger().Error("fail to save asgard vault", "error", err, "hash", observedTxID)
			}
			mgr.K.SetObservedTxOutVoter(ctx, outVoter)
			mgr.K.SetObservedTxInVoter(ctx, voter)
			ctx.Logger().Info("vault added funds back", "vault", activeAsgards[0].PubKey, "amount", coin.Amount)

			txIds = append(txIds, observedTxID)
		}
	}

	requeueDanglingActions(ctx, mgr, txIds)
}

func migrateStoreV114(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v114", "error", err)
		}
	}()

	activeAsgards, err := mgr.Keeper().GetAsgardVaultsByStatus(ctx, ActiveVault)
	if err != nil || len(activeAsgards) == 0 {
		ctx.Logger().Error("fail to get active asgard vaults", "error", err)
		return
	}

	// https://mayanode.mayachain.info/mayachain/tx/details/83AEC95CE5BC2B4AE8835B23DE57ACDF14CC3B30B00095ADB9D7278840CABD2D
	coin := common.NewCoin(common.DASHAsset, cosmos.NewUint(616040018514))
	activeAsgards[0].AddFunds(common.Coins{coin})
	if err := mgr.Keeper().SetVault(ctx, activeAsgards[0]); err != nil {
		ctx.Logger().Error("fail to save asgard vault", "error", err)
	}
}

func migrateStoreV115(ctx cosmos.Context, mgr *Mgrs) {}
func migrateStoreV116(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV117(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v116", "error", err)
		}
	}()

	txID := common.TxID("FD4CA0CEEE107E4A077BB178BC0A031EEB5BE6E55B6F529EE65C5A1A2487A621")

	newDestinationAddrString := "0xEf1C6F153afaf86424fd984728d32535902F1c3D"
	newDestinationAddr, err := common.NewAddress(newDestinationAddrString, mgr.GetVersion())
	if err != nil {
		ctx.Logger().Error("fail to parse address", "error", err)
		return
	}

	newMemo := fmt.Sprintf("=:ETH.ETH:%s:0/1/10:wr:20", newDestinationAddrString)
	iterator := mgr.Keeper().GetSwapQueueIterator(ctx)
	defer iterator.Close()
	for ; iterator.Valid(); iterator.Next() {
		var msg MsgSwap
		if err := mgr.Keeper().Cdc().Unmarshal(iterator.Value(), &msg); err != nil {
			ctx.Logger().Error("fail to fetch swap msg from queue", "error", err)
			continue
		}

		if msg.IsStreaming() && msg.Tx.ID.Equals(txID) {
			ss := strings.Split(string(iterator.Key()), "-")
			i, err := strconv.Atoi(ss[len(ss)-1])
			if err != nil {
				ctx.Logger().Error("fail to parse swap queue msg index", "key", iterator.Key(), "error", err)
				continue
			}

			if i != 0 {
				mgr.Keeper().RemoveSwapQueueItem(ctx, msg.Tx.ID, i)
				ctx.Logger().Info("Swap Queue Item Removed", "index", i)
			} else {
				oldMsg := msg
				msg.Destination = newDestinationAddr
				msg.Tx.Memo = newMemo
				if err := mgr.Keeper().SetSwapQueueItem(ctx, msg, 0); err != nil {
					ctx.Logger().Error("fail to save swap msg to queue", "error", err)
				}
				ctx.Logger().Info("Swap Queue Item Changed", "old msg", oldMsg, "new msg", msg)
			}
		}
	}
}

func migrateStoreV118(ctx cosmos.Context, mgr *Mgrs) {
	defer func() {
		if err := recover(); err != nil {
			ctx.Logger().Error("fail to migrate store to v118", "error", err)
		}
	}()

	fundCacaoPoolV118(ctx, mgr)
}

func fundCacaoPoolV118(ctx cosmos.Context, mgr *Mgrs) {
	var err error
	if err = consolidateStagenetFundsV118(ctx, mgr); err != nil {
		ctx.Logger().Error("fail to consolidate stagenet funds", "error", err)
	}

	threeMillionCacao := uint64(3_000_000_0000000000)
	coinsToCacaoPool := common.NewCoin(common.BaseNative, cosmos.NewUint(threeMillionCacao))
	if err = mgr.Keeper().SendFromModuleToModule(ctx, ReserveName, CACAOPoolName, common.NewCoins(coinsToCacaoPool)); err != nil {
		ctx.Logger().Error("fail to move coins from Reserve to CACAOPool", "error", err)
	}
}

func consolidateStagenetFundsV118(ctx cosmos.Context, mgr *Mgrs) error {
	asgard := mgr.Keeper().GetModuleAccAddress(AsgardName)
	reserve := mgr.Keeper().GetModuleAccAddress(ReserveName)
	leftover, err := cosmos.AccAddressFromBech32("tmaya13wrmhnh2qe98rjse30pl7u6jxszjjwl4fd6gwn")
	if err != nil {
		return fmt.Errorf("fail to parse leftover address")
	}

	type wallet struct {
		acc  cosmos.AccAddress
		coin cosmos.Coin
	}

	leftoverAmount := cosmos.ZeroUint()
	wallets := make([]wallet, 0)

	mgr.coinKeeper.IterateAllBalances(ctx, func(addr sdk.AccAddress, coin sdk.Coin) bool {
		if coin.Denom == common.BaseAsset().Native() && !coin.Amount.IsZero() {
			if addr.Equals(leftover) {
				leftoverAmount = cosmos.NewUintFromBigInt(coin.Amount.BigInt())
			}

			wallets = append(wallets, wallet{
				acc:  addr,
				coin: coin,
			})
		}

		return false
	})

	// move everything to leftover except asgard, reserve
	for _, wallet := range wallets {
		amount := wallet.coin
		if !amount.IsZero() {
			if wallet.acc.Equals(asgard) || wallet.acc.Equals(reserve) || wallet.acc.Equals(leftover) {
				continue
			}
			ctx.Logger().Info("Sending cacao", "from", wallet.acc, "to", leftover, "amount", amount)
			if err = mgr.Keeper().SendCoins(ctx, wallet.acc, leftover, cosmos.NewCoins(wallet.coin)); err != nil {
				return fmt.Errorf("fail to send coins: %w", err)
			}

			leftoverAmount = leftoverAmount.Add(cosmos.NewUintFromBigInt(wallet.coin.Amount.BigInt()))
		}
	}

	desiredSupply := cosmos.NewUint(100_000_000_0000000000)
	supplyCoin := mgr.coinKeeper.GetSupply(ctx, common.BaseAsset().Native())
	supply := cosmos.NewUintFromBigInt(supplyCoin.Amount.BigInt())
	// all coins sent to leftover, amounts are calculated
	if supply.GT(desiredSupply) {
		excess := supply.Sub(desiredSupply)

		if leftoverAmount.LT(excess) {
			ctx.Logger().Error("Unable to burn desired excess, module accounts have more than accounts", "excess", excess, "leftover", leftoverAmount)
			excess = leftoverAmount
		}

		ctx.Logger().Info("Sending cacao excess amount to mayachain module for burning", "from", leftover, "amount", excess)
		if err = mgr.Keeper().SendFromAccountToModule(ctx, leftover, ModuleName, common.NewCoins(common.NewCoin(common.BaseNative, excess))); err != nil {
			return fmt.Errorf("fail to send from leftover to mayachain module: %w", err)
		}
		if err = mgr.Keeper().BurnFromModule(ctx, ModuleName, common.NewCoin(common.BaseNative, excess)); err != nil {
			return fmt.Errorf("fail to burn excess coins: %w", err)
		}
	}
	return nil
}
